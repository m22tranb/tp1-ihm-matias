using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    public float speed = 3.0f;
    public InputAction activateRotation ;
    public bool rotation = false;

    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();
        activateRotation.started += ctx => rotation = !rotation;;
        // activateRotation.performed += ctx => transform.Rotate(0,speed,0); Debug.Log ("touche R pressée");
        // activateRotation.canceled += ctx => transform.Rotate(0,0,0);;
    }

    // Update is called once per frame
    void Update()
    {
        if(rotation){
            transform.Rotate(0,speed,0);
        }
        
    }
}
