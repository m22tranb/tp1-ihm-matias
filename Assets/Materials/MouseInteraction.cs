using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private  Rigidbody rigidbody1;
    private Renderer rend;
    private Color color;
    //Detect if a click occurs
    

    // Start is called before the first frame update
    void Start()
    {
        rigidbody1 = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        rigidbody1.AddForce(Camera.main.transform.forward * 10000);
        Debug.Log(name + " Game Object Clicked!");
    }

    public void  OnPointerEnter(PointerEventData pointerEventData)
    {
        color = rend.material.color;
        rend.material.color = Color.red;
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        rend.material.color = color;
    }
}
