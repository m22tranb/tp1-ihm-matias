using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public InputAction createCube;
    public GameObject aCopier;
    private bool throwCube = false;
    private int intensite = 200;

    // Start is called before the first frame update
    void Start()
    {
        createCube.Enable();
        createCube.performed += ctx =>  shoot();
    }

    private void shoot()
    {
        GameObject cube = Instantiate(aCopier); 
        cube.GetComponent<Renderer>().material.color = Color.yellow;
        cube.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 3;
        cube.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * intensite, ForceMode.Impulse);
    }
    // Update is called once per frame
    void Update()
    {

            
    }
    
}
